const express = require('express');
const app = express();
const path = require('path');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const request = require('request');

app.use(favicon('teemofavicon.png'));
app.use(express.static('dist'));
app.use(bodyParser.json()); // Needed for JSON data in POST requests

// Allow CORS
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

// API
let uri = 'mongodb+srv://dbmanager:vnxdMKAclbDTguW8@teemo-guide-cluster-bpkwo.mongodb.net/teemo-guide-db?retryWrites=true';
let championgg_api_key = '89ec9a81192a3b457f5cd0417c84ff3e';

app.get('/api/matchup', function(req, res) {
	const champion = req.query.champion;
	const opponent = req.query.opponent;
	console.log(req.query);

	MongoClient.connect(uri, { useNewUrlParser: true }, function(err, db) {
		if (err) {
			console.log(err);
			return res.status(500).send();
		} else {
			const matchups = db.db("teemo-guide-db").collection("matchups");

			// Find record. If does not exist, return 404.
			matchups.findOne({
				"champion": champion,
				"opponent": opponent
			}, (err, matchup) => {
				console.log(matchup);
				matchup !== null ? res.status(200).json(matchup) : res.status(404).send("Matchup not found");
			})
		}
	})
});

app.post('/api/matchup', function(req, res) {
	const champion = req.query.champion;
	const opponent = req.query.opponent;
	const matchup = req.body;

	console.log(champion);
	console.log(opponent);
	console.log(matchup);

	MongoClient.connect(uri, function(err, db) {
		const matchups = db.db("teemo-guide-db").collection("matchups");

		// Find record where champion and opponent match query. If does not exist, upsert new document.
		matchups.updateOne({
			"champion": champion,
			"opponent": opponent
		}, {
			$set: {
				"champion": champion,
				"opponent": opponent,
				"matchup": matchup
			}
		},  {
			upsert: true
		}, (err, response) => {
			if (err) {
				console.log(err);
				return res.status(500).send(err);
			} 
			console.log(response.result);
			if (response.result.ok === 1) {
				response.result.nModified === 1 ? res.status(200).send() : res.status(201).send()
			}
		});
	});
});

// GET to champion.gg matchup data
app.get('/api/winrate', function(req, res) {
	let url = 'http://api.champion.gg/champion/' + req.query.champion + '/matchup/' + req.query.opponent + '?&api_key=' + championgg_api_key;
	console.log(url);
	request.get(url, (error, response, body) => {
		if (error || response.headers['content-type'] !== 'application/json; charset=utf-8') {
			return res.status(500).send("Champion.gg API is down");
		}

		let json = JSON.parse(body);
		if ("error" in json) {
			return res.status(404).send("No matchup data available");
		}
		res.send(JSON.parse(body)[0].winRate.toString());
	})
});

// 404
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	res.json({
		message: err.message,
		error: err
	});
});

app.listen(8080, '0.0.0.0');
